# Create a function that takes an integer as an argument and returns "Even" for even numbers or "Odd" for odd numbers.


## Solution:

```go
package main

import "fmt"

func main() {
	fmt.Println(EvenOrOdd(3))
}

func EvenOrOdd(number int) string {
	switch number % 2 {
	case 0:
		return "Even"
	default:
		return "Odd"
	}
}
```

# Create a combat function that takes the player's current health and the amount of damage recieved, and returns the player's new health. Health can't be less than 0.

## Solution:

```go
package main

import (
	"fmt"
)

func main() {
	fmt.Println(combat(100, 33))
	fmt.Println(combat(100, 133))
}

func combat(health, damage float64) float64 {
	if hit := health - damage; hit > 0 {
		return hit
	}
	return 0
}
```
