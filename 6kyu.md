# You are given an array(list) strarr of strings and an integer k. Your task is to return the first longest string consisting of k consecutive strings taken in the array.

## Examples:

```
strarr = ["tree", "foling", "trashy", "blue", "abcdef", "uvwxyz"], k = 2

Concatenate the consecutive strings of strarr by 2, we get:

treefoling   (length 10)  concatenation of strarr[0] and strarr[1]
folingtrashy ("      12)  concatenation of strarr[1] and strarr[2]
trashyblue   ("      10)  concatenation of strarr[2] and strarr[3]
blueabcdef   ("      10)  concatenation of strarr[3] and strarr[4]
abcdefuvwxyz ("      12)  concatenation of strarr[4] and strarr[5]

Two strings are the longest: "folingtrashy" and "abcdefuvwxyz".
The first that came is "folingtrashy" so
longest_consec(strarr, 2) should return "folingtrashy".

In the same way:
longest_consec(["zone", "abigail", "theta", "form", "libe", "zas", "theta", "abigail"], 2) --> "abigailtheta"
```

> n being the length of the string array, if n = 0 or k > n or k <= 0 return "".

## Note:

consecutive strings : follow one after another without an interruption

## Solution:

```go
package main

import "fmt"

func main() {
	fmt.Println(LongestConsec([]string{"tree", "foling", "trashy", "blue", "abcdef", "uvwxyz"}, 2))
	fmt.Println(LongestConsec([]string{"ejjjjmmtthh", "zxxuueeg", "aanlljrrrxx", "dqqqaaabbb", "oocccffuucccjjjkkkjyyyeehh"}, 1))
	fmt.Println(LongestConsec([]string{"itvayloxrp", "wkppqsztdkmvcuwvereiupccauycnjutlv", "vweqilsfytihvrzlaodfixoyxvyuyvgpck"}, 2))
	fmt.Println(LongestConsec([]string{"zone", "abigail", "theta", "form", "libe", "zas", "theta", "abigail"}, 9))
	fmt.Println(LongestConsec([]string{"zone", "abigail", "theta", "form", "libe", "zas", "theta", "abigail"}, -1))
}

func LongestConsec(strarr []string, k int) string {
	if len(strarr) == 0 || k > len(strarr) || k <= 0 {
		return ""
	}
	var newstrarr []string
	var sums string
	for i := 0; i < len(strarr)-k+1; i++ {
		for j := i; j < k+i; j++ {
			sums += strarr[j]
		}
		newstrarr = append(newstrarr, sums)
		sums = ""
	}
	var max string = newstrarr[0]
	var maxidx int
	for idx, elem := range newstrarr {
		if len(max) < len(elem) {
			max = elem
			maxidx = idx
		}
	}
	return newstrarr[maxidx]
}
```

# Given a positive number n > 1 find the prime factor decomposition of n.

The result will be a string with the following form :

```
"(p1**n1)(p2**n2)...(pk**nk)"
```

with the p(i) in increasing order and n(i) empty if n(i) is 1.

## Example:

```
n = 86240 should return "(2**5)(5)(7**2)(11)"
```

## Solution:

```go
package main

import (
	"fmt"
	. "math"
)

func main() {
	a := int(Pow(86240, 3))
	fmt.Println(genAnswerLine(getPrimes(a)))
}

func getPrimes(n int) (res []int) {
	z := &n
	for i := 2; i <= n; i++ {
		if n%i == 0 {
			res = append(res, i)
			*z = n / i
			i--
		}
	}
	return res
}

func genAnswerLine(arr []int) (s string) {
	var j uint = 1
	for i := 1; i < len(arr); i++ {
		if arr[i-1] == arr[i] {
			j++
		} else if j == 1 {
			s += fmt.Sprintf("(%d)", arr[i-1])
		} else {
			s += fmt.Sprintf("(%d**%d)", arr[i-1], j)
			j = 1
		}
	}
	if j > 1 {
		s += fmt.Sprintf("(%d**%d)", arr[len(arr)-1], j)
	} else {
		s += fmt.Sprintf("(%d)", arr[len(arr)-1])
	}
	return s
}
```
